package com.exam.marlena.controller;

import com.exam.marlena.domain.Message;
import com.exam.marlena.service.MailSenderService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class SendMailController {

  private final MailSenderService mailSenderService;

  @Autowired
  public SendMailController(final MailSenderService mailSenderService) {
    this.mailSenderService = mailSenderService;
  }

  @GetMapping(value = "/send")
  public String sendMail(@RequestParam(name = "mail") final String mail) {
    final Message message = Message.builder().withContent("Hi! I have passed exam! Yupi!")
        .withSubject("This is exam message...")
        .withTo(mail)
        .build();
    return mailSenderService.send(message);
  }
}


